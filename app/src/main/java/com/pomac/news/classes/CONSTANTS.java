package com.pomac.news.classes;

public class CONSTANTS {


    public static final String BASE_URL = "https://api.nytimes.com/svc/mostpopular/v2/";


    /*
     ** Shared Preferences Constants
     */
    public static final String SHARED_PREFERENCES_NAME = "pomac.news";
    public static final String AUTHENTICATION_TOKEN = "AUTHENTICATION_TOKEN";


}
