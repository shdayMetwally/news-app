package com.pomac.news.classes.rest.models.responses;

import com.pomac.news.classes.rest.models.beans.Article;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ArticlesResponse extends BaseResponse{
    private int num_results;
    private List<Article> results;
}