package com.pomac.news.classes.rest;

import android.content.Context;
import android.content.ContextWrapper;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class ConnectivityStatus extends ContextWrapper {

    public ConnectivityStatus(Context base) {
        super(base);
    }



    public static boolean isInternetAvailable() {
        try {
            InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }
}