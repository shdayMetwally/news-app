package com.pomac.news.classes.rest.interceptors;

import android.content.Context;

import com.google.gson.Gson;
import com.pomac.news.classes.rest.ConnectivityStatus;
import com.pomac.news.classes.rest.models.responses.BaseResponse;
import com.pomac.news.classes.rest.NetworkEvent;
import com.pomac.news.classes.rest.NetworkState;
import com.pomac.news.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class NetworkInterceptor implements Interceptor {

    /*
     * Step 1: We pass the context instance here,
     * since we need to get the ConnectivityStatus
     * to check if there is internet.
     * */
    private final Context context;
    private final NetworkEvent networkEvent;

    public NetworkInterceptor(Context context) {
        this.context = context;
        this.networkEvent = NetworkEvent.getInstance();
    }

    @Override
    public @NotNull Response intercept(Chain chain) {
        Request request = chain.request();

        /*
         * Step 2: We check if there is internet
         * available in the device. If not, pass
         * the networkState as NO_INTERNET.
         * */

        if (!ConnectivityStatus.isInternetAvailable()) {
            networkEvent.publish(NetworkState.NetworkStateBuilder.aNetworkState()
                    .setCode(NetworkState.NetworkStateConstant.NO_INTERNET)
                    .setMessage(context.getResources().getString(R.string.errorInternetError))
                    .build());

        } else {
            try {
                /*
                 * Step 3: Get the response code from the
                 * request. In this scenario we are only handling
                 * unauthorised and server unavailable error
                 * scenario
                 * */
                BaseResponse baseResponse = null;
                Response response = chain.proceed(request);
                ResponseBody body = response.body();
                String bodyString = body.string();
                if (response.code() == 200) {
                    MediaType contentType = body.contentType();
                    return response.newBuilder().body(ResponseBody.create(bodyString, contentType)).build();
                }
                try {
                    JSONObject jsonObject = new JSONObject(bodyString);
                    Gson gson = new Gson();
                    baseResponse = gson.fromJson(jsonObject.toString(), BaseResponse.class);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                switch (response.code()) {
                    case 400:
                        networkEvent.publish(NetworkState.NetworkStateBuilder.aNetworkState()
                                .setCode(NetworkState.NetworkStateConstant.BAD_REQUEST)
                                .setMessage((baseResponse != null && baseResponse.getFault().getFaultstring() != null) ?
                                        baseResponse.getFault().getFaultstring() :
                                        context.getResources().getString(R.string.badRequest))
                                .build());
                        break;
                    case 401:
                        networkEvent.publish(NetworkState.NetworkStateBuilder.aNetworkState()
                                .setCode(NetworkState.NetworkStateConstant.UNAUTHORISED)
                                .setMessage((baseResponse != null && baseResponse.getFault().getFaultstring() != null) ?
                                        baseResponse.getFault().getFaultstring() :
                                        context.getResources().getString(R.string.error_login_expired))
                                .build());
                        break;

                    case 503:
                        networkEvent.publish(NetworkState.NetworkStateBuilder.aNetworkState()
                                .setCode(NetworkState.NetworkStateConstant.NO_RESPONSE)
                                .setMessage((baseResponse != null && baseResponse.getFault().getFaultstring() != null) ?
                                        baseResponse.getFault().getFaultstring() :
                                        context.getResources().getString(R.string.errorAPI))
                                .build());
                        break;
                }

                MediaType contentType = body.contentType();
                return response.newBuilder().body(ResponseBody.create(bodyString, contentType)).build();
            } catch (IOException e) {
                Timber.e(e);

            }
        }
        return null;
    }
}