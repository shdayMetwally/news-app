package com.pomac.news.classes;

import android.app.Application;
import android.content.Context;

import com.pomac.news.BuildConfig;

import timber.log.Timber;

public class MainApplication extends Application {

    private static Context mContext;
    Timber.DebugTree debugTree = new Timber.DebugTree();


    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;


        if (BuildConfig.DEBUG) {
            Timber.plant(debugTree);
        }
    }


}