package com.pomac.news.classes.rest;

import com.pomac.news.classes.CONSTANTS;
import com.pomac.news.classes.MainApplication;
import com.pomac.news.classes.rest.interceptors.AuthenticationInterceptor;
import com.pomac.news.classes.rest.interceptors.NetworkInterceptor;
import com.pomac.news.classes.rest.models.responses.ArticlesResponse;


import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class Client {
    private static Client INSTANCE;
    private final ApiInterface apiInterface;


    public Client() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message));
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        NetworkInterceptor networkInterceptor = new NetworkInterceptor(MainApplication.getContext());
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(networkInterceptor)
                .addInterceptor(new AuthenticationInterceptor())
                .addInterceptor(httpLoggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CONSTANTS.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        apiInterface = retrofit.create(ApiInterface.class);

    }

    public static Client getINSTANCE() {
        if (null == INSTANCE) {
            INSTANCE = new Client();
        }
        return INSTANCE;
    }

    public Observable<ArticlesResponse> requestArticles(int period) {
        return apiInterface.requestArticles(period);
    }


}

