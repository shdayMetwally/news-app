package com.pomac.news.classes.rest.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Media implements Parcelable {
    private String type;
    @SerializedName("media-metadata")
    List<MediaMetadata> media_metadata;

    protected Media(Parcel in) {
        type = in.readString();
        media_metadata = in.createTypedArrayList(MediaMetadata.CREATOR);
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeTypedList(media_metadata);
    }
}