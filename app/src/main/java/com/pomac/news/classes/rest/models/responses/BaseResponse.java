package com.pomac.news.classes.rest.models.responses;


import com.pomac.news.classes.rest.models.beans.Fault;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseResponse {
    /**
     * message : تم استلام البيانات بنجاح
     * success : true
     */

    private Fault fault;


    public boolean isSuccess() {
        return fault == null;
    }
}
