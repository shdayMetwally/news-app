package com.pomac.news.classes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.pomac.news.classes.rest.models.beans.Article;
import com.pomac.news.databinding.ListItemArticleBinding;

import java.util.ArrayList;
import java.util.List;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticlesAdapterViewHolder> {

    private List<Article> mData = new ArrayList<>();
    private ArticlesAdapterOnClickHandler mClickHandler;
    Context context;

    public ArticlesAdapter() {
    }

    public void setItemClickListener(ArticlesAdapterOnClickHandler clickHandler) {
        mClickHandler = clickHandler;
    }

    @NonNull
    @Override
    public ArticlesAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        ListItemArticleBinding binding = ListItemArticleBinding.inflate(LayoutInflater.from(context), parent, false);
        return new ArticlesAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesAdapterViewHolder holder, int position) {
        Article article = mData.get(position);

        holder.mBinding.content.setText(article.getTitle());

    }

    @Override
    public int getItemCount() {
        if (null == mData) return 0;
        return mData.size();
    }

    public void setData(List<Article> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public interface ArticlesAdapterOnClickHandler {
        void onArticleItemClick(Article article);
    }

    public class ArticlesAdapterViewHolder extends RecyclerView.ViewHolder {

        ListItemArticleBinding mBinding;

        public ArticlesAdapterViewHolder(ListItemArticleBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            itemView.setOnClickListener(this::onItemClick);
        }

        public void onItemClick(View view) {
            int adapterPosition = getBindingAdapterPosition();
            Article article = mData.get(adapterPosition);
            if (mClickHandler != null)
                mClickHandler.onArticleItemClick(article);
        }
    }
}