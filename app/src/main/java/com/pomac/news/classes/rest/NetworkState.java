package com.pomac.news.classes.rest;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NetworkState {

    private NetworkStateConstant code;
    private String message;


    public enum NetworkStateConstant {
        NO_INTERNET, NO_RESPONSE, BAD_REQUEST, UNAUTHORISED
    }

    public static final class NetworkStateBuilder {
        private NetworkStateConstant code;
        private String message;

        private NetworkStateBuilder() {
        }

        public static NetworkStateBuilder aNetworkState() {
            return new NetworkStateBuilder();
        }

        public NetworkStateBuilder setCode(NetworkStateConstant code) {
            this.code = code;
            return this;
        }

        public NetworkStateBuilder setMessage(String message) {
            this.message = message;
            return this;
        }

        public NetworkState build() {
            NetworkState networkState = new NetworkState();
            networkState.setCode(code);
            networkState.setMessage(message);
            return networkState;
        }
    }
}

