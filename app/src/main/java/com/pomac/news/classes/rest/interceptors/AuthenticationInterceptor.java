package com.pomac.news.classes.rest.interceptors;


import com.pomac.news.classes.MainApplication;
import com.pomac.news.classes.UserPreferences;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationInterceptor implements Interceptor {
    private final UserPreferences userPreferences;


    public AuthenticationInterceptor() {
        userPreferences = new UserPreferences(MainApplication.getContext());
    }

    @Override
    public @NotNull Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("api-key", userPreferences.getAuthenticationToken())
                .build();


        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .url(url);

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }


}
