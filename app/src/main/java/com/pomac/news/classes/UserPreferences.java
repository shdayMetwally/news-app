package com.pomac.news.classes;

import android.content.Context;
import android.content.SharedPreferences;



import static com.pomac.news.classes.CONSTANTS.AUTHENTICATION_TOKEN;
import static com.pomac.news.classes.CONSTANTS.SHARED_PREFERENCES_NAME;



public class UserPreferences {
    SharedPreferences sharedPreferences;
    Context mContext;

    // shared pref mode
    int PRIVATE_MODE = 0;

    public UserPreferences(Context context) {
        this.mContext = context;
        sharedPreferences = mContext.getSharedPreferences(SHARED_PREFERENCES_NAME, PRIVATE_MODE);


    }


    public String getAuthenticationToken() {
        return sharedPreferences.getString(AUTHENTICATION_TOKEN, null);
    }

    public void setAuthenticationToken(String authenticationToken) {
        sharedPreferences.edit().putString(AUTHENTICATION_TOKEN, authenticationToken).apply();
    }


    public void removeUser(String tag) {
        sharedPreferences.edit().putString(AUTHENTICATION_TOKEN, null).apply();

    }


}