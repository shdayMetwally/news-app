package com.pomac.news.classes.rest.models.beans;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Fault {
    private int id;
    private String faultstring;
}