package com.pomac.news.classes.rest;


import com.pomac.news.classes.rest.models.responses.ArticlesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;


interface ApiInterface {

    @GET("viewed/{period}.json")
    Observable<ArticlesResponse> requestArticles(@Path("period") int period);


}
