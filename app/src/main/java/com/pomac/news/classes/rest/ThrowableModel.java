package com.pomac.news.classes.rest;

public class ThrowableModel {
    private Throwable throwable;
    private final boolean displayable;
    private final boolean userDisplayable;


    public ThrowableModel(Throwable throwable, boolean displayable) {
        this.throwable = throwable;
        this.displayable = displayable;
        this.userDisplayable = true;
    }

    public ThrowableModel(String throwable, boolean displayable) {
        this.throwable = new Throwable(throwable);
        this.displayable = displayable;
        this.userDisplayable = true;
    }


    public Throwable getThrowable() {
        return throwable;
    }


}
