package com.pomac.news.classes.rest.models.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Article implements Parcelable {
    private String title;
    List<Media> media;
    @SerializedName("abstract")
    private String description;
    private String published_date;


    protected Article(Parcel in) {
        title = in.readString();
        media = in.createTypedArrayList(Media.CREATOR);
        description = in.readString();
        published_date = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(media);
        dest.writeString(description);
        dest.writeString(published_date);
    }
}