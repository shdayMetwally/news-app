package com.pomac.news.mvvm._Base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pomac.news.classes.rest.Client;
import com.pomac.news.classes.rest.models.responses.BaseResponse;
import com.pomac.news.classes.rest.ThrowableModel;

import io.reactivex.disposables.CompositeDisposable;
import lombok.Getter;
import lombok.Setter;
import timber.log.Timber;

@Setter
@Getter
public class BaseViewModel extends ViewModel {


    public Client client = Client.getINSTANCE();
    private CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<Boolean> onLoadingLiveData = new MutableLiveData<>();
    private MutableLiveData<ThrowableModel> onApiErrorLiveData = new MutableLiveData<>();

    protected boolean takeWhile(BaseResponse response) {
        Timber.d("takeWhile");
        if (!response.isSuccess()) {
            getOnApiErrorLiveData().setValue(new ThrowableModel(response.getFault().getFaultstring(), true));
            return false;
        } else
            return true;

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }

}
