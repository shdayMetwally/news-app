package com.pomac.news.mvvm.articles_Master_Details;

import androidx.lifecycle.MutableLiveData;

import com.pomac.news.classes.rest.models.responses.ArticlesResponse;
import com.pomac.news.classes.rest.ThrowableModel;
import com.pomac.news.mvvm._Base.BaseViewModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class ArticlesViewModel extends BaseViewModel {

    private MutableLiveData<ArticlesResponse> articlesResponseMutableLiveData = new MutableLiveData<>();


    public MutableLiveData<ArticlesResponse> requestArticles(int period) {
        getDisposables().add(Observable.just(period)
                .doOnNext(__ -> getOnLoadingLiveData().setValue(true))
                .observeOn(Schedulers.io())
                .flatMap(data -> client.requestArticles(data))
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> getOnLoadingLiveData().setValue(false))
                .takeWhile(this::takeWhile)
                .subscribe(response -> articlesResponseMutableLiveData.setValue(response),
                        throwable -> getOnApiErrorLiveData().setValue(new ThrowableModel(throwable, false))));
        return articlesResponseMutableLiveData;

    }


}