package com.pomac.news.mvvm.articles_Master_Details;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pomac.news.classes.rest.models.beans.Article;
import com.pomac.news.databinding.FragmentArticleDetailsBinding;

import org.jetbrains.annotations.NotNull;


public class ArticleDetailsFragment extends Fragment {

    public static final String ARTICLE = "ARTICLE";
    private Article mArticle;


    public ArticleDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARTICLE)) {
            mArticle = getArguments().getParcelable(ARTICLE);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FragmentArticleDetailsBinding mBinding = FragmentArticleDetailsBinding.inflate(inflater, container, false);

        if (mArticle != null) {
            mBinding.articleDescription.setText(mArticle.getDescription());
            mBinding.articlePublishDate.setText(mArticle.getPublished_date());

        }

        return mBinding.getRoot();
    }
}