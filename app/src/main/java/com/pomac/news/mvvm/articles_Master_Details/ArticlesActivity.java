package com.pomac.news.mvvm.articles_Master_Details;

import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.pomac.news.classes.adapters.ArticlesAdapter;
import com.pomac.news.classes.rest.models.beans.Article;
import com.pomac.news.classes.rest.models.responses.ArticlesResponse;
import com.pomac.news.mvvm._Base.BaseActivity;
import com.pomac.news.R;
import com.pomac.news.databinding.ActivityArticlesBinding;


public class ArticlesActivity extends BaseActivity {

    private boolean mTwoPane;
    ActivityArticlesBinding mBinding;

    ArticlesAdapter mArticlesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityArticlesBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        //Assuming that we get token from login and saved it in SharedPreferences
        if (userPreferences.getAuthenticationToken() == null)
            userPreferences.setAuthenticationToken("aZ7Hx6AQgAHDYjQ6c8nzIqUqPptGgb1r");

        setSupportActionBar(mBinding.toolbar);
        mBinding.toolbar.setTitle(getTitle());


        if (mBinding.viewArticles.itemDetailContainer != null) {
            mTwoPane = true;
        }

        //ViewModel Setup
        ArticlesViewModel mViewModel = new ViewModelProvider(this).get(ArticlesViewModel.class);
        mViewModel.getOnApiErrorLiveData().observe(this, this::onApiError);
        mViewModel.getOnLoadingLiveData().observe(this, this::onLoading);
        mViewModel.getArticlesResponseMutableLiveData().observe(this, this::onArticlesResponse);


        mArticlesAdapter = new ArticlesAdapter();

        mArticlesAdapter.setItemClickListener(this::onArticleClick);

        mBinding.viewArticles.articles.setAdapter(mArticlesAdapter);

        mViewModel.requestArticles(7);
    }

    private void onArticlesResponse(ArticlesResponse response) {
        mArticlesAdapter.setData(response.getResults());

    }

    private void onArticleClick(Article article) {
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(ArticleDetailsFragment.ARTICLE, article);
            ArticleDetailsFragment fragment = new ArticleDetailsFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, ArticleDetailsActivity.class);
            intent.putExtra(ArticleDetailsFragment.ARTICLE, article);
            startActivity(intent);
        }
    }


}