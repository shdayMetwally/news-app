package com.pomac.news.mvvm.articles_Master_Details;

import android.os.Bundle;

import com.pomac.news.classes.rest.models.beans.Article;
import com.pomac.news.mvvm._Base.BaseActivity;
import com.pomac.news.R;
import com.pomac.news.databinding.ActivityArticleDetailsBinding;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.ActionBar;


public class ArticleDetailsActivity extends BaseActivity {

    ActivityArticleDetailsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityArticleDetailsBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        setSupportActionBar(mBinding.detailToolbar);

        mBinding.detailToolbar.setNavigationOnClickListener(view -> finish());

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.

            Article article = getIntent().getParcelableExtra(ArticleDetailsFragment.ARTICLE);
            mBinding.toolbarLayout.setTitle(article.getTitle());
            if (!article.getMedia().isEmpty() && !article.getMedia().get(0).getMedia_metadata().isEmpty())
                Picasso.get().load(article.getMedia().get(0).getMedia_metadata().get(0).getUrl()).into(mBinding.toolbarBackground);
            Bundle arguments = new Bundle();
            arguments.putParcelable(ArticleDetailsFragment.ARTICLE, article);
            ArticleDetailsFragment fragment = new ArticleDetailsFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }
    }


}