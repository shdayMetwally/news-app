package com.pomac.news.mvvm._Base;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.pomac.news.classes.rest.NetworkEvent;
import com.pomac.news.classes.rest.ThrowableModel;
import com.pomac.news.classes.UserPreferences;
import com.pomac.news.R;

import net.bohush.geometricprogressview.GeometricProgressView;
import net.bohush.geometricprogressview.TYPE;

import timber.log.Timber;

public class BaseFragment extends Fragment {

    GeometricProgressView progressBar;
    AlertDialog errorAlertDialog;
    public UserPreferences userPreferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userPreferences = new UserPreferences(getContext());


    }

    @Override
    public void onResume() {
        super.onResume();
        NetworkEvent.getInstance().register(this,
                networkState -> {
                    switch (networkState.getCode()) {
                        case NO_INTERNET:
                        case NO_RESPONSE:
                        case BAD_REQUEST:
                        case UNAUTHORISED:
                            displayErrorDialog(networkState.getMessage());
                            break;

                    }

                });
    }

    @Override
    public void onStop() {
        super.onStop();
        NetworkEvent.getInstance().unregister(this);
    }

    public void displayErrorDialog(String desc) {
        if (errorAlertDialog == null)
            errorAlertDialog = new AlertDialog.Builder(getContext()).setPositiveButton(getResources().getString(R.string.ok),
                    (dialogInterface, i) -> dialogInterface.dismiss()).create();
        errorAlertDialog.setTitle(getResources().getString(R.string.error));
        errorAlertDialog.setMessage(desc);
        errorAlertDialog.setCancelable(false);
        if (!errorAlertDialog.isShowing())
            errorAlertDialog.show();


    }

    public void onApiError(ThrowableModel throwableModel) {
        Timber.e(throwableModel.getThrowable());
    }

    public void onLoading(boolean isLoading) {
        if (isLoading)
            hideKeyboard();

        if (progressBar == null)
            createProgressBar();
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        disableAction(isLoading);

    }

    private void createProgressBar() {
        ViewGroup layout = (ViewGroup) this.getView();

        progressBar = new GeometricProgressView(getContext());
        progressBar.setType(TYPE.KITE);
        progressBar.setNumberOfAngles(30);
        progressBar.setFigurePaddingInDp(1);
        progressBar.setColor(getContext().getResources().getColor(R.color.purple_700));
        progressBar.setDuration(1000);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        RelativeLayout rl = new RelativeLayout(getContext());

        rl.setGravity(Gravity.CENTER);
        rl.addView(progressBar);

        layout.addView(rl, params);

    }

    public void disableAction(boolean loading) {
        if (loading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
